﻿namespace TaxiPlannerAPI.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using TaxiPlannerAPI.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<TaxiPlannerAPI.Models.TaxiPlannerAPIContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = false;
        }

        protected override void Seed(TaxiPlannerAPI.Models.TaxiPlannerAPIContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            Role employee = new Role()
            {
                
                name = "employee"
            };

            Role emp2 = new Role()
            {
               
                name = "employee"
            };
            Role superadmin = new Role()
            {
                
                name = "superadmin"
            };
            Role hr = new Role()
            {

                name = "hr"
            };

            Role approver = new Role()
            {

                name = "approver"
            };

            Role delegated_approver = new Role()
            {

                name = "delegated_approver"
            };

            context.Roles.AddOrUpdate(

                employee,
                emp2,
                hr,
                superadmin,
                approver,
                delegated_approver

            );

        }
        //the flow:
       //Add departments and teams without manager/team ID and then add employees. then update tables.
    }
}
