--Booking slots by department
SELECT bs.booking_id, bs.date_time, bs.status, e.name, d.dept_name , ee.name as managername
FROM BookingSlots bs  left join Bookings b on bs.booking_id=b.booking_id
left join Employees e on b.emp_id= e.emp_id
left  join CostCenters c on e.costcenter_id=c.costcenter_id
left  join Departments d on c.dept_id = d.dept_id
left join Employees ee on d.manager_id=ee.emp_id
order by booking_id

select e.emp_id, e.name, r.name as rolename,rs.region_name,e.reporting_line_id,ee.name as reportingname,  c.costcenter_name,d.dept_name from Employees e
left join EmployeeRoles er on e.emp_id=er.emp_id
left join Roles r on er.role_id=r.role_id
left join Regions rs on e.region_id=rs.region_id
left join CostCenters c on e.costcenter_id=c.costcenter_id
left join Departments d on c.dept_id = d.dept_id
left join Employees ee on e.reporting_line_id = ee.emp_id
order by e.emp_id

select  * from Employees

--AUTH
select r.name, er.role_id from EmployeeRoles er join Roles r on er.role_id = r.role_id
 where er.emp_id = 2 AND (er.expiry_date IS NULL OR er.expiry_date > CURRENT_TIMESTAMP)
order by er.role_id DESC

--get booking slots by reporting line
SELECT bs.booking_id, bs.date_time, bs.status, bs.reason, bs.approved_by_id, e.name, d.dept_name , ee.name as directSuperior1
FROM BookingSlots bs  left join Bookings b on bs.booking_id=b.booking_id
left join Employees e on b.emp_id= e.emp_id
left  join CostCenters c on e.costcenter_id=c.costcenter_id
left  join Departments d on c.dept_id = d.dept_id
left join Employees ee on e.reporting_line_id= ee.emp_id


order by booking_id

select ee.email, ee.name from Employees e left join Employees ee on e.reporting_line_id= ee.emp_id
where e.emp_id=6
							