namespace TaxiPlannerAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Bookings",
                c => new
                    {
                        booking_id = c.Int(nullable: false, identity: true),
                        timestamp_booked = c.DateTime(nullable: false),
                        emp_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.booking_id);
            
            CreateTable(
                "dbo.BookingSlots",
                c => new
                    {
                        booking_id = c.Int(nullable: false),
                        date_time = c.DateTime(nullable: false),
                        status = c.Int(nullable: false),
                        reason = c.String(),
                        evaluation_timestamp = c.DateTime(),
                        approved_by_id = c.Int(),
                    })
                .PrimaryKey(t => new { t.booking_id, t.date_time })
                .ForeignKey("dbo.Bookings", t => t.booking_id, cascadeDelete: true)
                .Index(t => t.booking_id);
            
            CreateTable(
                "dbo.NotificationTokens",
                c => new
                    {
                        emp_id = c.Int(nullable: false),
                        token = c.String(nullable: false, maxLength: 500),
                        device_id = c.String(),
                        os = c.String(),
                    })
                .PrimaryKey(t => new { t.emp_id, t.token });
            
            CreateTable(
                "dbo.Regions",
                c => new
                    {
                        region_name = c.String(nullable: false, maxLength: 128),
                        estimated_price = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.region_name);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        role_id = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.role_id);
            
            CreateTable(
                "dbo.ScheduleTimes",
                c => new
                    {
                        schedule_type = c.Int(nullable: false),
                        time = c.DateTime(nullable: false),
                        schedule_name = c.String(),
                    })
                .PrimaryKey(t => new { t.schedule_type, t.time });
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BookingSlots", "booking_id", "dbo.Bookings");
            DropIndex("dbo.BookingSlots", new[] { "booking_id" });
            DropTable("dbo.ScheduleTimes");
            DropTable("dbo.Roles");
            DropTable("dbo.Regions");
            DropTable("dbo.NotificationTokens");
            DropTable("dbo.BookingSlots");
            DropTable("dbo.Bookings");
        }
    }
}
