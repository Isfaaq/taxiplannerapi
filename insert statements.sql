DELETE FROM BookingSlots
DELETE FROM Bookings

select * from Bookings
select * from BookingSlots
select * from Employees
select * from EmployeeRoles
select * from Roles
select * from Departments
select * from CostCenters
select * from Regions
select * from ScheduleTimes

SET IDENTITY_INSERT [dbo].[Regions] ON 
GO
INSERT [dbo].[Permissions] ([permission_id], [user_id], [role_id]) VALUES (1, N'Savanne', 230)
GO
INSERT [dbo].[Regions] ([region_id], [region_name], [estimated_price]) VALUES (2, N'Plaine Wilhems', 120)
GO
INSERT [dbo].[Regions] ([region_id], [region_name], [estimated_price]) VALUES (3, N'Pamplemousses', 1000)
GO
SET IDENTITY_INSERT [dbo].[Regions] OFF

INSERT INTO Roles VALUES('employee');
INSERT INTO Roles VALUES('approver');
INSERT INTO Roles VALUES('hr');
INSERT INTO Roles VALUES('superadmin');
INSERT INTO Roles VALUES('manager');

--insert departments without manager ID first
GO
SET IDENTITY_INSERT [dbo].[Departments] ON 
GO
INSERT [dbo].[Departments] ([dept_id], [dept_name]) VALUES (1, N'Department 1')
GO
INSERT [dbo].[Departments] ([dept_id], [dept_name]) VALUES (2, N'Department 2')
GO
INSERT [dbo].[Departments] ([dept_id], [dept_name]) VALUES (3, N'Department 3')
GO
INSERT [dbo].[Departments] ([dept_id], [dept_name]) VALUES (4, N'Department 4')
--GO
--INSERT [dbo].[Departments] ([dept_id], [dept_name], [manager_id]) VALUES (5, N'Payroll', 5)
--GO
SET IDENTITY_INSERT [dbo].[Departments] OFF
GO


GO
SET IDENTITY_INSERT [dbo].[CostCenters] ON 
GO
INSERT [dbo].[CostCenters] ([costcenter_id], [costcenter_name], [dept_id]) VALUES (1, N'CC 1', 1) 
GO
INSERT [dbo].[CostCenters] ([costcenter_id], [costcenter_name], [dept_id]) VALUES (2, N'CC 2', 1)
GO
INSERT [dbo].[CostCenters] ([costcenter_id], [costcenter_name], [dept_id]) VALUES (3, N'CC 3', 1)
GO
INSERT [dbo].[CostCenters] ([costcenter_id], [costcenter_name], [dept_id]) VALUES (4, N'CC 4', 2)
GO
INSERT [dbo].[CostCenters] ([costcenter_id], [costcenter_name], [dept_id]) VALUES (5, N'CC 5', 3)
GO
INSERT [dbo].[CostCenters] ([costcenter_id], [costcenter_name], [dept_id]) VALUES (6, N'CC 6', 4)
GO
INSERT [dbo].[CostCenters] ([costcenter_id], [costcenter_name], [dept_id]) VALUES (7, N'CC 7', 4)

SET IDENTITY_INSERT [dbo].[CostCenters] OFF
GO

--add reporting line id later
GO
SET IDENTITY_INSERT [dbo].[Employees] ON 
GO
INSERT [dbo].[Employees] ([emp_id], [name], [email], [password], [address_line], [start_date],[region_id], [costcenter_id]) VALUES (1, N'Reeshal Rittoo', N'rr@gmail.com', N'12345', N'Royal Road Souillac',CAST(N'2017-07-11T00:00:00.000' AS DateTime) ,1, 1) --mananger RR
GO
INSERT [dbo].[Employees] ([emp_id], [name], [email], [password], [address_line], [start_date],[region_id], [costcenter_id]) VALUES (2, N'Yogesh Mohall', N'ym@gmail.com', N'12345', N'Royal Road Souillac', CAST(N'2017-07-11T00:00:00.000' AS DateTime) , 2, 2) --under RR
GO
INSERT [dbo].[Employees] ([emp_id], [name], [email], [password], [address_line], [start_date],[region_id], [costcenter_id]) VALUES (3, N'Isfaq', N'ig@gmail.com', N'12345', N'Royal Road Souillac',CAST(N'2016-07-11T00:00:00.000' AS DateTime) , 3, 5)  --hr
GO
INSERT [dbo].[Employees] ([emp_id], [name], [email], [password], [address_line], [start_date],[region_id], [costcenter_id]) VALUES (4, N'Tavihs', N'tb@gmail.com', N'12345', N'pl',CAST(N'2016-07-11T00:00:00.000' AS DateTime) , 1, 6) --under hh
GO
INSERT [dbo].[Employees] ([emp_id], [name], [email], [password], [address_line], [start_date],[region_id], [costcenter_id]) VALUES (5, N'Pajani', N'pr@gmail.com', N'12345', N'Rhill',CAST(N'2016-07-11T00:00:00.000' AS DateTime) , 2, 3)  --under RR
GO
INSERT [dbo].[Employees] ([emp_id], [name], [email], [password], [address_line], [start_date],[region_id], [costcenter_id]) VALUES (6, N'hania', N'hh@gmail.com', N'12345', N'gb',CAST(N'2016-07-11T00:00:00.000' AS DateTime) , 3, 7) --under hh
GO
INSERT [dbo].[Employees] ([emp_id], [name], [email], [password], [address_line], [start_date],[region_id], [costcenter_id]) VALUES (7, N'Jaya', N'jr@gmail.com', N'12345', N'chemin grenier',CAST(N'2016-07-11T00:00:00.000' AS DateTime) , 3, 1) --under RR
GO
INSERT [dbo].[Employees] ([emp_id], [name], [email], [password], [address_line], [start_date],[region_id], [costcenter_id]) VALUES (8, N'Kidir', N'kp@gmail.com', N'12345', N'rh',CAST(N'2016-07-11T00:00:00.000' AS DateTime) , 2, 4)  --mananger kr
GO
INSERT [dbo].[Employees] ([emp_id], [name], [email], [password], [address_line], [start_date],[region_id], [costcenter_id]) VALUES (9, N'heeya', N'hy@gmail.com', N'12345', N'rh',CAST(N'2016-07-11T00:00:00.000' AS DateTime) , 1, 1)  --superadmin
GO
INSERT [dbo].[Employees] ([emp_id], [name], [email], [password], [address_line], [start_date],[region_id]) VALUES (10, N'steven', N'so@gmail.com', N'12345', N'rh',CAST(N'2016-07-11T00:00:00.000' AS DateTime) , 3)  --above manager, not in any dpt
GO
SET IDENTITY_INSERT [dbo].[Employees] OFF
GO

--GO
--SET IDENTITY_INSERT [dbo].[EmployeeRoles] ON 
--GO
INSERT [dbo].[EmployeeRoles] ([emp_id], [role_id]) VALUES (1,2)
GO
UPDATE Departments SET manager_id= 1 where dept_id=1
GO
INSERT [dbo].[EmployeeRoles] ([emp_id], [role_id]) VALUES (2,1)
GO
INSERT [dbo].[EmployeeRoles] ([emp_id], [role_id]) VALUES (3,3)
GO
INSERT [dbo].[EmployeeRoles] ([emp_id], [role_id]) VALUES (4,1)
GO
INSERT [dbo].[EmployeeRoles] ([emp_id], [role_id]) VALUES (5,1)
GO
INSERT [dbo].[EmployeeRoles] ([emp_id], [role_id]) VALUES (6,2)
GO
UPDATE Departments SET manager_id= 6 where dept_id=4
GO
INSERT [dbo].[EmployeeRoles] ([emp_id], [role_id]) VALUES (7,1)
INSERT [dbo].[EmployeeRoles] ([emp_id], [role_id]) VALUES (8,2)
INSERT [dbo].[EmployeeRoles] ([emp_id], [role_id]) VALUES (9,4)
INSERT [dbo].[EmployeeRoles] ([emp_id], [role_id]) VALUES (10,4)

INSERT [dbo].[EmployeeRoles] ([emp_id], [role_id],[expiry_date],[delegator_id]) VALUES (2,5, '2020-07-21T21:30:00.000',1) --delegating yogesh reeeshal's position
GO
UPDATE Departments SET manager_id= 8 where dept_id=2

--reporting line now
--managers report to steven
update Employees set reporting_line_id= 10 where emp_id= 1
update Employees set reporting_line_id= 10 where emp_id= 6
update Employees set reporting_line_id= 10 where emp_id= 8
--reporting to rr
update Employees set reporting_line_id=1  where emp_id= 2
update Employees set reporting_line_id=1  where emp_id= 5
update Employees set reporting_line_id=1  where emp_id= 7

--reporting to hh
update Employees set reporting_line_id=6  where emp_id= 4

--reporrting superadmin
update Employees set reporting_line_id=9  where emp_id= 9
update Employees set reporting_line_id=9  where emp_id= 10

GO
SET IDENTITY_INSERT [dbo].[Bookings] ON 
GO
INSERT [dbo].[Bookings] ([booking_id], [timestamp_booked], [emp_id]) VALUES (1, CAST(N'2020-07-10T21:30:00.000' AS DateTime), 1)
GO
INSERT [dbo].[Bookings] ([booking_id], [timestamp_booked], [emp_id]) VALUES (2, CAST(N'2020-07-10T14:40:14.380' AS DateTime), 2)
GO
INSERT [dbo].[Bookings] ([booking_id], [timestamp_booked], [emp_id]) VALUES (3, CAST(N'2020-07-11T21:21:59.780' AS DateTime), 3)
GO
INSERT [dbo].[Bookings] ([booking_id], [timestamp_booked], [emp_id]) VALUES (4, CAST(N'2020-07-11T21:28:52.013' AS DateTime), 4)
GO
INSERT [dbo].[Bookings] ([booking_id], [timestamp_booked], [emp_id]) VALUES (5, CAST(N'2020-07-11T22:34:43.270' AS DateTime), 4)
GO
INSERT [dbo].[Bookings] ([booking_id], [timestamp_booked], [emp_id]) VALUES (7, CAST(N'2020-07-11T22:38:59.713' AS DateTime), 5)
GO
INSERT [dbo].[Bookings] ([booking_id], [timestamp_booked], [emp_id]) VALUES (9, CAST(N'2020-07-11T22:42:40.840' AS DateTime), 6)
GO
SET IDENTITY_INSERT [dbo].[Bookings] OFF
GO
INSERT [dbo].[BookingSlots] ([booking_id], [date_time], [status], [reason], [evaluation_timestamp]) VALUES (1, CAST(N'2020-07-20T20:30:00.000' AS DateTime), 0, N'vgbook', NULL)
GO
INSERT [dbo].[BookingSlots] ([booking_id], [date_time], [status], [reason], [evaluation_timestamp]) VALUES (1, CAST(N'2020-07-20T20:30:00.000' AS DateTime), 0, N'vgbook2', NULL)
GO
INSERT [dbo].[BookingSlots] ([booking_id], [date_time], [status], [reason], [evaluation_timestamp]) VALUES (2, CAST(N'2020-07-22T20:30:00.000' AS DateTime), 0, N'dgtest', NULL)
GO
INSERT [dbo].[BookingSlots] ([booking_id], [date_time], [status], [reason], [evaluation_timestamp]) VALUES (2, CAST(N'2020-07-21T21:30:00.000' AS DateTime), 0, N'dg', NULL)
GO
INSERT [dbo].[BookingSlots] ([booking_id], [date_time], [status], [reason], [evaluation_timestamp]) VALUES (3, CAST(N'2020-07-20T21:30:00.000' AS DateTime), 0, N'pdg', NULL)
GO
INSERT [dbo].[BookingSlots] ([booking_id], [date_time], [status], [reason], [evaluation_timestamp]) VALUES (4, CAST(N'2020-07-22T20:30:00.000' AS DateTime), 0, N'so', NULL)
GO
INSERT [dbo].[BookingSlots] ([booking_id], [date_time], [status], [reason], [evaluation_timestamp]) VALUES (4, CAST(N'2020-07-23T20:30:00.000' AS DateTime), 0, N'so', NULL)
GO
INSERT [dbo].[BookingSlots] ([booking_id], [date_time], [status], [reason], [evaluation_timestamp]) VALUES (4, CAST(N'2020-07-24T21:30:00.000' AS DateTime), 0, N'so', NULL)
GO
INSERT [dbo].[BookingSlots] ([booking_id], [date_time], [status], [reason], [evaluation_timestamp]) VALUES (5, CAST(N'2020-07-23T20:30:00.000' AS DateTime), 0, N'so', NULL)
GO
INSERT [dbo].[BookingSlots] ([booking_id], [date_time], [status], [reason], [evaluation_timestamp]) VALUES (5, CAST(N'2020-07-24T21:30:00.000' AS DateTime), 0, N'so', NULL)
GO
INSERT [dbo].[BookingSlots] ([booking_id], [date_time], [status], [reason], [evaluation_timestamp]) VALUES (5, CAST(N'2020-07-21T20:30:00.000' AS DateTime), 0, N'so', NULL)
GO
INSERT [dbo].[BookingSlots] ([booking_id], [date_time], [status], [reason], [evaluation_timestamp]) VALUES (7, CAST(N'2020-08-11T21:30:00.000' AS DateTime), 0, N'sk', NULL)
GO
INSERT [dbo].[BookingSlots] ([booking_id], [date_time], [status], [reason], [evaluation_timestamp]) VALUES (7, CAST(N'2020-08-12T20:30:00.000' AS DateTime), 0, N'sk', NULL)
GO
INSERT [dbo].[BookingSlots] ([booking_id], [date_time], [status], [reason], [evaluation_timestamp]) VALUES (7, CAST(N'2020-08-13T21:30:00.000' AS DateTime), 0, N'sk', NULL)
GO
INSERT [dbo].[BookingSlots] ([booking_id], [date_time], [status], [reason], [evaluation_timestamp]) VALUES (9, CAST(N'2020-08-11T20:30:00.000' AS DateTime), 1, N'pb', NULL)
GO
INSERT [dbo].[BookingSlots] ([booking_id], [date_time], [status], [reason], [evaluation_timestamp]) VALUES (9, CAST(N'2020-08-12T20:30:00.000' AS DateTime), 1, N'pb', NULL)
GO
INSERT [dbo].[BookingSlots] ([booking_id], [date_time], [status], [reason], [evaluation_timestamp]) VALUES (9, CAST(N'2020-08-13T20:30:00.000' AS DateTime), 1, N'pb', NULL)
GO


INSERT INTO ScheduleTimes VALUES(0,'21:30:00','BookTaxi')
INSERT INTO ScheduleTimes VALUES(0,'20:30:00','BookTaxi')
INSERT INTO ScheduleTimes VALUES(0,'19:30:00','BookTaxi')
INSERT INTO ScheduleTimes VALUES(1,'15:30:00','Deadline')
