namespace TaxiPlannerAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _new : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Bookings",
                c => new
                    {
                        booking_id = c.Int(nullable: false, identity: true),
                        timestamp_booked = c.DateTime(nullable: false),
                        emp_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.booking_id)
                .ForeignKey("dbo.Employees", t => t.emp_id, cascadeDelete: true)
                .Index(t => t.emp_id);
            
            CreateTable(
                "dbo.BookingSlots",
                c => new
                    {
                        booking_id = c.Int(nullable: false),
                        date_time = c.DateTime(nullable: false),
                        status = c.Int(nullable: false),
                        reason = c.String(),
                        evaluation_timestamp = c.DateTime(),
                        approved_by_id = c.Int(),
                    })
                .PrimaryKey(t => new { t.booking_id, t.date_time })
                .ForeignKey("dbo.Bookings", t => t.booking_id, cascadeDelete: true)
                .ForeignKey("dbo.Employees", t => t.approved_by_id)
                .Index(t => t.booking_id)
                .Index(t => t.approved_by_id);
            
            CreateTable(
                "dbo.Employees",
                c => new
                    {
                        emp_id = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false),
                        email = c.String(nullable: false),
                        password = c.String(nullable: false),
                        address_line = c.String(nullable: false),
                        start_date = c.DateTime(nullable: false),
                        end_date = c.DateTime(),
                        region_id = c.Int(nullable: false),
                        costcenter_id = c.Int(),
                        reporting_line_id = c.Int(),
                    })
                .PrimaryKey(t => t.emp_id)
                .ForeignKey("dbo.CostCenters", t => t.costcenter_id)
                .ForeignKey("dbo.Employees", t => t.reporting_line_id)
                .ForeignKey("dbo.Regions", t => t.region_id, cascadeDelete: true)
                .Index(t => t.region_id)
                .Index(t => t.costcenter_id)
                .Index(t => t.reporting_line_id);
            
            CreateTable(
                "dbo.CostCenters",
                c => new
                    {
                        costcenter_id = c.Int(nullable: false, identity: true),
                        costcenter_name = c.String(nullable: false),
                        dept_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.costcenter_id)
                .ForeignKey("dbo.Departments", t => t.dept_id, cascadeDelete: true)
                .Index(t => t.dept_id);
            
            CreateTable(
                "dbo.Departments",
                c => new
                    {
                        dept_id = c.Int(nullable: false, identity: true),
                        dept_name = c.String(nullable: false),
                        manager_id = c.Int(),
                    })
                .PrimaryKey(t => t.dept_id)
                .ForeignKey("dbo.Employees", t => t.manager_id)
                .Index(t => t.manager_id);
            
            CreateTable(
                "dbo.EmployeeRoles",
                c => new
                    {
                        emprole_id = c.Int(nullable: false, identity: true),
                        emp_id = c.Int(nullable: false),
                        role_id = c.Int(nullable: false),
                        start_date = c.DateTime(),
                        expiry_date = c.DateTime(),
                        delegator_id = c.Int(),
                    })
                .PrimaryKey(t => t.emprole_id)
                .ForeignKey("dbo.Employees", t => t.emp_id, cascadeDelete: true)
                .ForeignKey("dbo.Roles", t => t.role_id, cascadeDelete: true)
                .Index(t => t.emp_id)
                .Index(t => t.role_id);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        role_id = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.role_id);
            
            CreateTable(
                "dbo.Regions",
                c => new
                    {
                        region_id = c.Int(nullable: false, identity: true),
                        region_name = c.String(nullable: false),
                        estimated_price = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.region_id);
            
            CreateTable(
                "dbo.ScheduleTimes",
                c => new
                    {
                        schedule_type = c.Int(nullable: false),
                        time = c.DateTime(nullable: false),
                        schedule_name = c.String(),
                    })
                .PrimaryKey(t => new { t.schedule_type, t.time });
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BookingSlots", "approved_by_id", "dbo.Employees");
            DropForeignKey("dbo.Employees", "region_id", "dbo.Regions");
            DropForeignKey("dbo.Employees", "reporting_line_id", "dbo.Employees");
            DropForeignKey("dbo.EmployeeRoles", "role_id", "dbo.Roles");
            DropForeignKey("dbo.EmployeeRoles", "emp_id", "dbo.Employees");
            DropForeignKey("dbo.Employees", "costcenter_id", "dbo.CostCenters");
            DropForeignKey("dbo.Departments", "manager_id", "dbo.Employees");
            DropForeignKey("dbo.CostCenters", "dept_id", "dbo.Departments");
            DropForeignKey("dbo.Bookings", "emp_id", "dbo.Employees");
            DropForeignKey("dbo.BookingSlots", "booking_id", "dbo.Bookings");
            DropIndex("dbo.EmployeeRoles", new[] { "role_id" });
            DropIndex("dbo.EmployeeRoles", new[] { "emp_id" });
            DropIndex("dbo.Departments", new[] { "manager_id" });
            DropIndex("dbo.CostCenters", new[] { "dept_id" });
            DropIndex("dbo.Employees", new[] { "reporting_line_id" });
            DropIndex("dbo.Employees", new[] { "costcenter_id" });
            DropIndex("dbo.Employees", new[] { "region_id" });
            DropIndex("dbo.BookingSlots", new[] { "approved_by_id" });
            DropIndex("dbo.BookingSlots", new[] { "booking_id" });
            DropIndex("dbo.Bookings", new[] { "emp_id" });
            DropTable("dbo.ScheduleTimes");
            DropTable("dbo.Regions");
            DropTable("dbo.Roles");
            DropTable("dbo.EmployeeRoles");
            DropTable("dbo.Departments");
            DropTable("dbo.CostCenters");
            DropTable("dbo.Employees");
            DropTable("dbo.BookingSlots");
            DropTable("dbo.Bookings");
        }
    }
}
