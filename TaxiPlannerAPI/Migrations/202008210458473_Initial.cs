namespace TaxiPlannerAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Bookings", "emp_id", "dbo.Employees");
            DropForeignKey("dbo.CostCenters", "dept_id", "dbo.Departments");
            DropForeignKey("dbo.Departments", "manager_id", "dbo.Employees");
            DropForeignKey("dbo.Employees", "costcenter_id", "dbo.CostCenters");
            DropForeignKey("dbo.EmployeeRoles", "emp_id", "dbo.Employees");
            DropForeignKey("dbo.EmployeeRoles", "role_id", "dbo.Roles");
            DropForeignKey("dbo.Employees", "reporting_line_id", "dbo.Employees");
            DropForeignKey("dbo.Employees", "region_id", "dbo.Regions");
            DropForeignKey("dbo.BookingSlots", "approved_by_id", "dbo.Employees");
            DropIndex("dbo.Bookings", new[] { "emp_id" });
            DropIndex("dbo.BookingSlots", new[] { "approved_by_id" });
            DropIndex("dbo.Employees", new[] { "region_id" });
            DropIndex("dbo.Employees", new[] { "costcenter_id" });
            DropIndex("dbo.Employees", new[] { "reporting_line_id" });
            DropIndex("dbo.CostCenters", new[] { "dept_id" });
            DropIndex("dbo.Departments", new[] { "manager_id" });
            DropIndex("dbo.EmployeeRoles", new[] { "emp_id" });
            DropIndex("dbo.EmployeeRoles", new[] { "role_id" });
            CreateTable(
                "dbo.NotificationTokens",
                c => new
                    {
                        emp_id = c.Int(nullable: false),
                        token = c.String(nullable: false, maxLength: 500),
                        device_id = c.String(),
                        os = c.String(),
                    })
                .PrimaryKey(t => new { t.emp_id, t.token });
            
            DropTable("dbo.Employees");
            DropTable("dbo.CostCenters");
            DropTable("dbo.Departments");
            DropTable("dbo.EmployeeRoles");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.EmployeeRoles",
                c => new
                    {
                        emprole_id = c.Int(nullable: false, identity: true),
                        emp_id = c.Int(nullable: false),
                        role_id = c.Int(nullable: false),
                        start_date = c.DateTime(),
                        expiry_date = c.DateTime(),
                        delegator_id = c.Int(),
                    })
                .PrimaryKey(t => t.emprole_id);
            
            CreateTable(
                "dbo.Departments",
                c => new
                    {
                        dept_id = c.Int(nullable: false, identity: true),
                        dept_name = c.String(nullable: false),
                        manager_id = c.Int(),
                    })
                .PrimaryKey(t => t.dept_id);
            
            CreateTable(
                "dbo.CostCenters",
                c => new
                    {
                        costcenter_id = c.Int(nullable: false, identity: true),
                        costcenter_name = c.String(nullable: false),
                        dept_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.costcenter_id);
            
            CreateTable(
                "dbo.Employees",
                c => new
                    {
                        emp_id = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false),
                        email = c.String(nullable: false),
                        password = c.String(nullable: false),
                        address_line = c.String(nullable: false),
                        start_date = c.DateTime(nullable: false),
                        end_date = c.DateTime(),
                        region_id = c.Int(nullable: false),
                        costcenter_id = c.Int(),
                        reporting_line_id = c.Int(),
                    })
                .PrimaryKey(t => t.emp_id);
            
            DropTable("dbo.NotificationTokens");
            CreateIndex("dbo.EmployeeRoles", "role_id");
            CreateIndex("dbo.EmployeeRoles", "emp_id");
            CreateIndex("dbo.Departments", "manager_id");
            CreateIndex("dbo.CostCenters", "dept_id");
            CreateIndex("dbo.Employees", "reporting_line_id");
            CreateIndex("dbo.Employees", "costcenter_id");
            CreateIndex("dbo.Employees", "region_id");
            CreateIndex("dbo.BookingSlots", "approved_by_id");
            CreateIndex("dbo.Bookings", "emp_id");
            AddForeignKey("dbo.BookingSlots", "approved_by_id", "dbo.Employees", "emp_id");
            AddForeignKey("dbo.Employees", "region_id", "dbo.Regions", "region_id", cascadeDelete: true);
            AddForeignKey("dbo.Employees", "reporting_line_id", "dbo.Employees", "emp_id");
            AddForeignKey("dbo.EmployeeRoles", "role_id", "dbo.Roles", "role_id", cascadeDelete: true);
            AddForeignKey("dbo.EmployeeRoles", "emp_id", "dbo.Employees", "emp_id", cascadeDelete: true);
            AddForeignKey("dbo.Employees", "costcenter_id", "dbo.CostCenters", "costcenter_id");
            AddForeignKey("dbo.Departments", "manager_id", "dbo.Employees", "emp_id");
            AddForeignKey("dbo.CostCenters", "dept_id", "dbo.Departments", "dept_id", cascadeDelete: true);
            AddForeignKey("dbo.Bookings", "emp_id", "dbo.Employees", "emp_id", cascadeDelete: true);
        }
    }
}
