# Taxi Planner API

## Installation steps

1. Ensure that you install packages from `packages.config`
2. Update connection string in `Web.config` (under `</configSections>`)
3. Seed the Database

> Enable-Migrations -Force

> Add-Migration Initial

> Update-Database